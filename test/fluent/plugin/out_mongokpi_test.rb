require 'helper'

class DataCounterOutputTest < Test::Unit::TestCase
  def setup
    Fluent::Test.setup
  end

  DEFAULT_CONFIG = %[
  ]

  DEFAULT_TAG = 'default_tag'

  def create_driver(conf = DEFAULT_CONFIG, tag = DEFAULT_TAG)
    Fluent::Test::BufferedOutputTestDriver.new(Fluent::MongoKpiOutput, tag).configure(conf)
  end

  def test_configure_default
    d = create_driver
    assert_equal 'localhost:27017', d.instance.address
    assert_equal 'kpidb', d.instance.db
    assert_equal nil, d.instance.connection_opts[:w]
    assert_equal nil, d.instance.connection_opts[:name]
    assert_equal nil, d.instance.connection_opts[:name]
    assert_equal nil, d.instance.connection_opts[:read]
    assert_equal nil, d.instance.connection_opts[:refresh_mode]
    assert_equal nil, d.instance.connection_opts[:refresh_interval]
    assert_equal false, d.instance.collections_opts[:capped]
    assert_equal nil, d.instance.collections_opts[:size]
    assert_equal nil, d.instance.collections_opts[:max]
  end

  def test_configure
    d = create_driver(DEFAULT_CONFIG + %[
      address 49.212.133.23:27019
      db testdb
      collection testColyyyymmdd
      write_concern 1
      name testreplica
      read secondary
      refresh_mode sync
      refresh_interval 1000
      capped_size 100
      capped_max 200
    ])
    assert_equal '49.212.133.23:27019', d.instance.address
    assert_equal 'testdb', d.instance.db
    assert_equal 'testColyyyymmdd', d.instance.collection
    assert_equal 1, d.instance.connection_opts[:w]
    assert_equal 'testreplica', d.instance.connection_opts[:name]
    assert_equal 'secondary', d.instance.connection_opts[:read]
    assert_equal 'sync', d.instance.connection_opts[:refresh_mode]
    assert_equal 1000, d.instance.connection_opts[:refresh_interval]
    assert_equal true, d.instance.collections_opts[:capped]
    assert_equal 100, d.instance.collections_opts[:size]
    assert_equal 200, d.instance.collections_opts[:max]
  end

  def test_format
    d = create_driver
    time = Time.parse("2112-09-03 01:23:45 UTC")
    d.emit({"gerogero" => "Let's get Rocking!", "site" => "yapoo"}, time)
    d.emit({"gerogero" => "Let's get Rocking!", "site" => "geegero"}, time)
    d.expect_format "\x93\xABdefault_tag\xCF\x00\x00\x00\x01\f[\xC8\xA1\x82\xA8gerogero\xB2Let's get Rocking!\xA4site\xA5yapoo\x93\xABdefault_tag\xCF\x00\x00\x00\x01\f[\xC8\xA1\x82\xA8gerogero\xB2Let's get Rocking!\xA4site\xA7geegero"
    # d.run

    # time = Time.parse("2011-01-02 13:14:15 UTC").to_i
    # d.emit({"a"=>1}, time)
    # d.emit({"a"=>2}, time)

    # d.expect_format %[2011-01-02T13:14:15Z\ttest\t{"a":1}\n]
    # d.expect_format %[2011-01-02T13:14:15Z\ttest\t{"a":2}\n]

    # d.run
  end

  def test_write
    d = create_driver

    # time = Time.parse("2011-01-02 13:14:15 UTC").to_i
    # d.emit({"a"=>1}, time)
    # d.emit({"a"=>2}, time)

    # ### FileOutput#write returns path
    # path = d.run
    # expect_path = "#{TMP_DIR}/out_file_test._0.log.gz"
    # assert_equal expect_path, path
  end
end



