# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "fluent-plugin-mongokpi"
  spec.version       = "0.0.2"
  spec.authors       = ["Hatayama Hideharu"]
  spec.email         = ["h.hiddy@gmail.com"]
  spec.description   = %q{Fluent BufferedOutput plugin: counting chunk, inserting counts to make kpi count on MongoDB}
  spec.summary       = spec.description
  spec.homepage      = "https://bitbucket.org/hidepiy/fluent-plugin-mongokpi"
  spec.license       = "APLv2"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_runtime_dependency "fluentd"
  spec.add_dependency "mongo", "1.9.2"
  spec.add_dependency "bson_ext", "1.9.2"
end

